import { STRING_TYPE } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { elementAt } from 'rxjs';

@Component({
  selector: 'app-studentlist',
  templateUrl: './studentlist.component.html',
  styleUrls: ['./studentlist.component.css']
})
export class StudentlistComponent  implements OnInit{
 students : any = [];
  constructor() {
      this.students=[
      {
        id:1,
        name :'Ajay',
        address : '89 Delhi',
        dob:'12/12/2000'
      },
      {
        id:2,
        name :'Jay',
        address : '89 Delhi',
        dob:'12/12/2000'
      },
      {
        id:3,
        name :'Ajay',
        address : '89 Delhi',
        dob:'12/12/2000'
      }
  
     ];
    
  }
  ngOnInit(): void {
    this.LoadStudents("");  
  }
  
  LoadStudents(searchText : string)
  { 
    this.FilterList(searchText);

}
filterRecords : any = []
   FilterList(searchText : any)
   {
    if(searchText=="")
    this.filterRecords = this.students;
    else 
    {
      this.filterRecords=[];
    console.log(this.students.length)
       this.students.forEach((element: any) => {
        {
          if(element.name.toLowerCase()==searchText)
          {
            this.filterRecords.push(element);
          }
          }
       }); 
      }  
   }
  }