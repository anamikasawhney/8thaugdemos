import { Component } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent {
  name : string ="Deepak";


  FunctionInvokedFromChildComp()
  {
    console.log("Called from child component")
  }

  FunctionInvokedFromChildCompWithSomeValue(msg : string)
  {
    console.log("Value Sent from child component   " + msg)
  }

}
