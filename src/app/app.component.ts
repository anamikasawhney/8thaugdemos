import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Day3';
  // Data Binding
  // One way  > Data can go from ts to html
  //1 . {{}} 2. Propertry Binding
  // Events > it happens from html to ts
  // 2 way
  call()
  {
    console.log("Called");
  }
}
