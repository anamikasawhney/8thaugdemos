import { Component , Input , Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent {
  @Input()
  data : string="";

  @Output()
  call = new EventEmitter();

  @Output()
  call1 = new EventEmitter<string>();

  SendValue(msg : string)
  {
    this.call1.emit(msg.toUpperCase());

  }





 
}
